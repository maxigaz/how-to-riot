\documentclass[12pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[margin=2.5cm, headheight=15pt]{geometry}
\usepackage[unicode, hidelinks]{hyperref}
\usepackage{xcolor}
\hypersetup{
	colorlinks,
	linkcolor={red!50!black},
	citecolor={blue!50!black},
	urlcolor={blue!80!black}
	% Prettier hyperlinks, thanks http://tex.stackexchange.com/posts/847/revisions
}
\usepackage[english]{babel}
\usepackage{microtype}
\usepackage[textsize=scriptsize]{todonotes}
\setlength{\marginparwidth}{2cm}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{listings}
\lstset{frame=tb, basicstyle=\ttfamily, aboveskip=3mm, belowskip=3mm, columns=flexible}

\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
\node[shape=circle,draw,inner sep=2pt] (char) {#1};}} % Draw circles around numbers (use \circled{<num>})

\setlength{\abovetopsep}{1ex} % Space below caption of tables

\title{Unofficial User's Manual for Riot (WIP)}
\author{Zsolt Szakács \\
	\texttt{szakacs dot zsolt at zoho dot com} \\
	\texttt{maxigaz colon matrix dot org}}
\date{\today}

\begin{document}

\maketitle

\subsection*{Preface}

I like the philosophy behind Riot and Matrix (which includes software openness, decentralisation, and interoperability), and I have been using them for a while for communication with my family, friends, business clients as well as finding like--minded people around the world—and I have been using it with great satisfaction.

So, I have decided to start a manual for end--users, which hopefully helps widen the adoption of this wonderful piece of software. I hope you'll enjoy it.

\tableofcontents

\section{Licence information}

This document is licenced under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution 4.0 International License}. Feel free to modify and use it for any purposes—all I ask for in return is give proper credit.

\section{The goal of this document}

\label{sec:goal} This user's manual has been written for general users, to familiarise them with what Riot can do to make their communications as pleasant as possible and help them get the most out of Riot.

\section{What is Riot?}

Riot is a free and open source client for the Matrix ecosystem. Some of its main features include the following:

\begin{itemize}
	\item The ability to create an unlimited number of chatrooms,
	\item the chatrooms can be private or public,
	\item excellent options for moderation (e.g.\ banning users or deleting messages),
	\item 1--to--1 audio and video calls and conference calls,
	\item file sharing,
	\item end--to--end encryption\footnote{When active, nobody except the participants can find out the content of your communications.} for messages, file attachments, and calls,
	\item showing preview for hyperlinks, if possible,
	\item can be connected with other platforms through bridges  (e.g.\ IRC, Slack, Gitter, Telegram, Twitter),
	\item cross--platform: available for GNU/Linux, Windows, OS X, Android and iOS.
\end{itemize}

\section{Registration}

In order to get the most out of Riot, it's worth registering on a homeserver. You can do this by one of the following ways.

\subsection{Riot Web}

Using the web version, simply go to \url{riot.im/app}. Once the page has loaded, look at the message appearing at the very top. (See No.\ \ref{itm:layout-topbar} in Figure \ref{fig:layout}.) Click on Register and fill out the details. (Default Server means \url{matrix.org} will be used as homeserver and \url{vector.im} as identity server. Select Custom Server if you want to use another one. If you're unsure, leave it as default.) Finally, if you provide an email address, check your inbox afterwards for verification.

For an unofficial list of available public homeservers, visit the following website: \url{https://www.hello-matrix.net/public_servers.php}

\subsection{Riot for Android/iOS}

The procedure is basically the same as above, except you simply launch the app and tap \textsc{Register}.

\section{Joining a room as a guest}

If for some reason you don't want to register, it's possible to join a room without the need to have a permanent account. For this, someone who has registered needs to create a room and, in the room settings, under `\textsc{Who can access this room?}', select `Anyone who knows the room's link, including guests'.

Then, on Riot Web, the URL pointing to the room in question can be shared with others through email, social media etc. If they open the link in a web browser supported by Riot, they are taken right to that room.

You can share a room ID from Riot Android/iOS by going to \textsc{Room Details} (press the sandwich button in the top right corner while in a room and press \textsc{Room Details}), then go to \textsc{Settings}, scroll down to \textsc{Addresses}, hold press on the address you want to share, and choose either \textsc{Copy Room ID} or \textsc{Copy Room Address}. (The latter shares a URL to \href{https://matrix.to/}{matrix.to}, which provides links that can be opened by various Matrix clients.)

\subsection{Riot Web}

Once you have selected a room from the room directory or opened the URL pointing to a room, for the first time, you can see the message seen on Figure \ref{fig:join-prompt}. (Under that, depending on the room settings set by the administrators of the room, you can see a preview of the room containing previous conversations and status messages). To join, simply click on the underlined word \textit{join}.

\begin{figure}
	\centering
	\includegraphics[scale=.85]{img/would-you-like-to-join.jpg}
	\caption{Message showing when you visit a room for the first time.}
	\label{fig:join-prompt}
\end{figure}

If you haven't used Riot on the device you're currently using, the message seen on Figure \ref{fig:guest-name} will pop up in the centre of the screen. Here, you need to type a display name you'd like others to see (or leave it as `Guest [number]`) and hit Enter or click on `Set'.

\begin{figure}
	\centering
	\includegraphics[scale=1]{img/guest-name.jpg}
	\caption{Setting your display name as a guest.}
	\label{fig:guest-name}
\end{figure}

\section{Riot for Android/iOS}

Using Riot on Android or iOS without logging in is not possible at the moment. As an alternative, you might want to try opening Riot Web in a web browser (such as Firefox), although be aware that Riot Web hasn't been optimised for mobile usage and you may encounter various issues not present when used from desktop.

\section{The layout of Riot}\label{sec:layout}

\begin{figure}
	\centering
	% \includegraphics[width=1\textwidth]{img/layout.jpg}
	\begin{tikzpicture}
		\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{img/layout.png}};
		\draw (9.4,10.95) node {\small{\circled{1}}}; % Not registered
		\draw (1.5,10.5) node {\small{\circled{2}}}; % Filter room names
		\draw (7.1,10.23) node {\small{\circled{3}}}; % Room details
		\draw[black,thick,rounded corners] (10.8,9.9) rectangle (12.55,10.55)
			(11.2,10.23) node {\small{\circled{4}}}; % Room settings and search buttons
		\draw[black,thick,rounded corners] (12.7,1.75) rectangle (15.7,10.65)
			(14.5,10.23) node {\small{\circled{5}}}; % Leave room, search and participants buttons
		\draw[black,thick,rounded corners] (0.10,1.75) rectangle (3.15,9.7)
			(1.5,2.2) node {\small{\circled{6}}}; % Room list side bar
		\draw[black,thick,rounded corners] (3.35,1.75) rectangle (12.5,9.7)
			(8,9.2) node {\small{\circled{7}}}; % Chat history
		\draw[black,thick,rounded corners] (0.10,0.05) rectangle (3.15,0.75)
			(2,.4) node {\small{\circled{8}}}; % Buttons below Room list
		\draw (6.4,.4) node {\small{\circled{9}}}; % Type a message…
		\draw[black,thick,rounded corners] (10.2,0.05) rectangle (12.5,0.75)
			(10.56,.4) node {\small{\circled{10}}}; % File sharing and call buttons
		% \draw[help lines] (0,0) grid (16,11); % Help lines: comment out when finished
	\end{tikzpicture}
	\caption{The layout of Riot after having joined a room}
	\label{fig:layout}
\end{figure}

On Figure \ref{fig:layout}\todo{Update screenshot}, you can see the layout of Riot Web after having joined a room. The numbers mark the following (where there are multiple elements, their functions are described from left to right respectively):

\begin{enumerate}
	\item \label{itm:layout-topbar} Message shown only when you're not logged in. % For translation purposes later: \textit{You are using Riot as a guest. Register or log in to access more rooms and features.}
	\item Search for rooms within the list of rooms you've visited. You can hide and show this whole left panel by clicking on the small arrow on the right.
	\item The avatar of the current room (if an image has been set, that's what appears, otherwise, the first character of the room ID or room title appears) its title and topic (if set). If you click on the title of the room the room settings will appear.
	\item Room settings and search room history.
	% \item \label{itm:layout-top-right-buttons} Select room member list (the number on the latter shows how many members the room currently has), files uploaded to the room, and notifications of someone mentioning you. Click any of these the second time to hide the sidebar. (When hidden, will show the sidebar again.)
	\item \label{itm:layout-right-sidebar} Right sidebar, which can show one of the following lists:
		\begin{itemize}
			\item Members of the current room. In the text box at the top, you can filter the members of the room by their display name or ID. By hovering the mouse on their name, their current status will appear (online, idle, or offline). Click on their name to see their account details including
				\begin{itemize}
					\item their avatar (click on it to see it in bigger size),
					\item display name,
					\item Matrix ID, and
					\item level of privilege.
				\end{itemize}
				In addition, here you can
				\begin{itemize}
					\item start a direct chat with this person (which will create an empty room which you'll join and they'll be invited to automatically) as well as
					\item verify or blacklist their devices (more on this in Section \ref{sec:e2ee}).
				\end{itemize}
			\item Browse files uploaded to the room. In the case of images, their previews will also appear. Click on the name of the file or the preview to download it or click on the ID of the sender or the date of submission to jump to its entry within the room history (e.g.\ to see its context).
			\item Notifications of others mentioning you in any rooms. Click on the name of the room, the ID of the sender of the message, or its date of submission to jump to that message in the history of the relevant room.
		\end{itemize}
		You can switch between these lists with the buttons above them. If you press the same button again, it will hide the sidebar. To show it again, click on the click on the \texttt{<} symbol.
	\item The list of the rooms you've visited.
		\begin{itemize}
			\item They fall into 5 different categories: \textsc{Favourites}, \textsc{People} (includes rooms with 2 or fewer members, so it's easier to find private messages), \textsc{Rooms} (includes rooms with 3 or more members), \textsc{Low Priority}, and \textsc{Historical} (includes those you've visited in the past but since left).
			\item Rooms can be added to the categories Favourite and Low Priority by dragging them with the mouse, and they can also be rearranged within these categories.
			\item Also, each category can be collapsed and opened by clicking on its name.
			\item If you hover the mouse on a room in the list, you'll be able to see a button with three dots on it. Click on it to change notification settings applied for that room.
		\end{itemize}
	\item Sent messages and other information in the room (e.g.\ somebody has joined or started a conference call).
	\begin{itemize}
		\item Messages containing your display name sent by someone else will automatically be highlighted (the colour of the text will be red).
		\item On the right, you can see read receipts in the form of tiny avatars, which indicate the point up to which others have read the messages. By hovering the mouse over them, you can see their Matrix ID. When there are many read receipts to be shown, they are collapsed (indicated by a number telling how many is hidden, such as +3). Click on any of them or the number to display all receipts and click on them again to hide.
	\end{itemize}
	\item \label{itm:layout-bottom-left-buttons} Start a new chat with somebody, go to the room directory (listing available public rooms), create a new empty room, and account settings.
	\item In the text box, you can enter your instant messages or slash commands (they can be found in Table \ref{tab:slashcommands}), and submit them by hitting Enter. Mention others' names to get their attention. \href{https://en.wikipedia.org/wiki/Markdown}{Markdown} is supported, which helps you easily format parts of your messages (e.g.\ make it bold, add hyperlinks and lists).
	\item Upload a file, start a voice call, and start a video call with the participant(s) of the room you're in currently.
	% \item Invite people to the current room.
\end{enumerate}

\section{Slash commands}
\label{sec:slashcommands}

Slash commands are powerful commands inspired by the ones available in IRC and similar networks. You use them by typing them in the message box and press Enter. You can also use autocompletion by pressing the Tab key. (Consequent presses of Tab will cicle through the available commands.)

You can see all the available slash commands in Table \ref{tab:slashcommands}. Some of them have effect only to the current room while others work always the same way regardless of what room you are in.

\begin{table}[htpb]
	\centering
	\caption{Available slash commands in Riot}
	\label{tab:slashcommands}
	\begin{tabular}{lp{6.2cm}}
		% \abovetopsep[1cm]
		\toprule
		Command & Description \\
		\midrule
		% \texttt{/join \#freenode\_\#channelname:matrix.org} & Join a room \\
		\verb+/ban <userid> [<reason>]+ & Ban someone in the room. \\
		\verb+/ddg <query>+ & Search query on DuckDuckGo, paste first result by pressing Tab. \\
		\verb+/deop <userid>+ & To be completed. \\
		\verb+/invite <userid>+ & Invite a user to the current room. \\
		\verb+/join #freenode_#<channelname>:matrix.org+ & Create a Matrix room bridged to the IRC channel you want to join (replace \verb+freenode+ with the IRC host and \verb+matrix.org+ with the Matrix homeserver). \\ % Todo: rewrite explanation so that it reflects all networks, move IRC-exclusive stuff elsewhere.
		\verb+/kick <userid> [<reason>]+ & Kick someone in the room. \\
		\verb+/op <userid> [<power_level>]+ & ?. \\
		\verb+/markdown <on|off>+ & ?. \\
		\verb+/me <message>+ & Send emote (a kind of status message) to room. If, for example your display name is John, and submit \verb+/me is happy+, the others will see the following: \verb+* John is happy+. \\
		\verb+/nick <display_name>+ & Change your display name. \\
		\verb+/op <userid> [<power_level>]+ & ?. \\
		\verb+/part [#alias:domain]]+ & ?. \\
		\verb+/tint <color1> [<color2>]+ & Apply colours to the left sidebar and buttons, given as hexadecimal values (e.g.\ \verb+#FF0000+ stands for red). You can find out the correct value for the colour you want to use with the help of a colour picker (\href{http://www.w3schools.com/colors/colors_picker.asp}{example}). \\
		\verb+/topic <topic>+ & ?. \\
		\verb+/unban <userid>+ & ?. \\
		\bottomrule
	\end{tabular}
\end{table}

\section{End--to--end encryption}
\label{sec:e2ee}

End--to--end encryption (E2EE) makes sure your messages, file attachments, and calls are encrypted in a way that nobody except the participants can know its content (i.e. even those who have direct access to the server are unable to decipher what is really stored on the it).

Starting from Riot V0.9.1, this feature is available to everyone using Riot. If you're interested in using it, please, read this whole section first to learn about what important things to look out for.

\subsection{Caveats}
\label{sub:caveats}

Whereas E2EE makes your communications more secure, there are a few things to bear in mind:
\begin{enumerate}
	\item For security reasons, it's impossible to disable E2EE for a room once it's enabled.
	\item Only members with a high level of privilege (i.e.\ admins and moderators) can enable E2EE.
	\item As of V0.9.1, new devices and users joining an encrypted room won't be able to decrypt its past content.
	\item Due to the reason mentioned just above, E2EE may not be ideal for public rooms and cases when you often switch to new devices (e.g.\ computers in a library).
	\item Only those Matrix clients will be able to decrypt the contents of the room that support E2EE.
\end{enumerate}

\subsection{Enable E2EE for a room}

To enable E2EE, open the settings of the room and click on \textsc{Enable encryption (warning: cannot be disabled again!)}, read the warning message, and click on OK.

\subsection{Device verification and blacklisting}
\label{sub:device-verification}

If you've enabled E2EE for the first time, you have probably noticed that there's a yellow warning sign in front of the message somebody has sent you. This sign means you haven't verified the other person's device yet, which is highly recommended before moving on because this way, you make sure it is really the person you want to communicate with on the other side, and also that there's nobody listening in.

Either click on this yellow sign or click on the user's name in the member list and then on the button \textsc{Verify\dots} below the device you want to verify. Then, a new pop--up message should appear.

In the pop--up message, look at the device name, ID, and key. Ask the other person to open the account settings (if you need help finding it, see No.\ \ref{itm:layout-bottom-left-buttons} in Section \ref{sec:layout}), scroll down, and read out these pieces of information---\emph{especially the device key} under the section \textsc{Cryptography}. (For extra security, let them read it out by other means of communication, such as talking in person or on the phone.) If what they say and what you can see on your side match, click on \textsc{I verify that the keys match}. Otherwise, you may want to immediately hang up (if there's an ongoing call), look for the device on in the right sidebar (after clicking on the name of the user), and \textsc{Blacklist} it.

\subsection{Device management}\label{sub:device-management}

In the account settings, look for the section \textsc{Devices}. You can rename your devices by clicking on their name, and you can also \textsc{Delete} them.

\section{Bridges and integrations}

Bridges allow you to connect your Matrix rooms to other communicational platforms, such as IRC, Slack, and Gitter. Thanks to this, your contacts can continue using their preferred platforms and keep in touch with you while you're on Matrix.

Integrations, on the other hand, can be considered bots that can have a wide range of functions invited to a room. For example, the RSS bot checks the RSS or Atom channels of blogs, news sites etc., and sends a notification to the room whenever a new post has been published. The GitHub bot allows you to interact with your project hosted there (e.g.\ create issues) or automatically announce activities about your repositories.

The availability of bridges and integrations depend on the configuration of the homeserver you're using. If you host your own server, you can set up any, even the ones written by you! See \href{http://matrix.org/docs/guides/application_services.html}{this guide on application services} and \href{http://matrix.org/docs/projects/try-matrix-now.html#application-services}{this list of available application services} for more information.

To add/remove bridges and integrations, go to the room settings and click on \textsc{Manage integrations}. There, select the one you'd like to use or change and follow the instructions.

After you configured a few, the section of integration management of the options should look similar to what is shown in Figure \ref{fig:integrations}.

\begin{figure}
	\centering
	\includegraphics[scale=.85]{img/integrations.png}
	\caption{Examples of bridges and integrations set up in a room.}
	\label{fig:integrations}
\end{figure}

\subsection{Example: joining an IRC channel}

Riot, while using an account registered on \url{matrix.org}, supports joining IRC channels out of the box! The current requirement, however, is that the channels need to be hosted on Freenode, Snoonet, OFTC, Mozilla or W3.

You can join an IRC channel by either opening \textsc{Integration management} mentioned above or submitting the slash command
\begin{lstlisting}
/join #_<irchost>_#<channelname>:matrix.org
\end{lstlisting}
which will create an empty Matrix room and bridge it together with the channel you want to join.

\section{Further information and support}

Do you have a question you haven't found your answer to? Feel free to visit one of the following Matrix rooms and ask away: \href{https://matrix.to/#/#riot:matrix.org}{Riot}, \href{https://matrix.to/#/#android:matrix.org}{Riot for Android}, \href{https://matrix.to/#/#ios:matrix.org}{Riot for iOS}.

\end{document}
