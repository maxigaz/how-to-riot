# How to Riot

An unofficial user’s manual to the [Matrix](http://matrix.org) client [Riot](https://riot.im), written in LaTeX. Intended for general users.

It’s currently WIP and only available in English. Once it’s done, I’m planning to translate it into other languages (namely Hungarian and Spanish).

## Download as PDF

If you only want to see the output as a PDF file, you can find it [here](https://gitlab.com/maxigaz/how-to-riot/raw/master/main-en.pdf).

## License information

Available under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

## Contribution

If you like my work, you can contribute to the future development of the manual the following ways:

- Create an issue and describe how the manual can be improved.
- [![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/maxigaz)
